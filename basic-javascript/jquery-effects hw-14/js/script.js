"use strict";

$(document).ready(function(){
    $("a[href*=#]").on("click", function(e){
        let anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 1200);
        e.preventDefault();
        return false;
    });
});

$(window).scroll(function() {
    if ($(window).scrollTop() > 800) {
        $('#button').addClass('show');
    } else {
        $('#button').removeClass('show');
    }
});

$('#button').on('click', function(e) {
    $('html, body').animate({scrollTop:0}, '4000');
});

$('#toggle').click(function () {
    $("#container-banner").slideToggle("slow");

});





 