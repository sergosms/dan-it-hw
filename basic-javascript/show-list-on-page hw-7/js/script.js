"use strict";

function nestedList(list) {
  function arrToUl(domElement, arr) {
      let ul = document.createElement('ul');
      let li;
      domElement.appendChild(ul);
      arr.map(function(item) {
          if (Array.isArray(item)) {
              arrToUl(li, item);
              return;
            }
          li = document.createElement('li');
          li.appendChild(document.createTextNode(item));
          ul.appendChild(li);
      });
  }
  let div = document.getElementById("myNestedList");
  arrToUl(div, list);
  let i = 3;
  let timerId = setInterval(function() {
      document.getElementById("timer").innerText = i;
      if (i === 0) {
      document.getElementById("myNestedList").classList.add("none");
      document.getElementById("timer").classList.add("none");
      clearInterval(timerId);
      list = [];
      }
      i--;
  }, 1000);
}
nestedList(["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);