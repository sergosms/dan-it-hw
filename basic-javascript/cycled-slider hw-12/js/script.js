"use strict";

let slides = document.querySelectorAll('.image-to-show');
let currentSlide = 0;
let slideInterval = setInterval(nextSlide,3000);

function nextSlide(){
    slides[currentSlide].className = 'image-to-show';
    currentSlide = (currentSlide+1)%slides.length;
    slides[currentSlide].className = 'image-to-show showing';
}
play.onclick = function playSlider() {
    slideInterval = setInterval(nextSlide,3000);
    play.setAttribute("disabled", "disabled")
}
pause.onclick = function pauseSlider () {
    clearInterval(slideInterval);
    play.removeAttribute("disabled")
}