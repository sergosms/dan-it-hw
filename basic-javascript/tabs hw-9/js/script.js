"use strict";

let ul = document.querySelector('.menu');
ul.addEventListener('click', function (e) {
    let data = e.target.dataset.tab;
    document.querySelector('.active').classList.remove('active');
    e.target.classList.add('active')
    document.querySelector('.active-list').classList.remove('active-list');
    document.querySelector(`[data-content = ${data}]`).classList.add('active-list');
});