"use strict";

let slides = document.querySelectorAll('.image-to-show');
let currentSlide = 0;
let slideInterval = setInterval(nextSlide,3000);

function nextSlide(){
    slides[currentSlide].className = 'image-to-show';
    currentSlide = (currentSlide+1)%slides.length;
    slides[currentSlide].className = 'image-to-show showing';
}
play.onclick = function playSlider() {
    slideInterval = setInterval(nextSlide,3000);
    play.setAttribute("disabled", "disabled")
}
pause.onclick = function pauseSlider () {
    clearInterval(slideInterval);
    play.removeAttribute("disabled")
}

let buttonSwitch = document.getElementById('change');

let body = document.getElementsByTagName('body')[0];
window.addEventListener('DOMContentLoaded', function () {
    if(localStorage.getItem('bgcolor') !== null) {
        let bgcolor = localStorage.getItem('bgcolor');
        body.style.background = bgcolor;
    }
    buttonSwitch.addEventListener("click", function () {
       body.style.background = "yellow";
        if (localStorage.getItem('bgcolor') === null){
            localStorage.setItem('bgcolor','yellow')
        }
        else {
            localStorage.removeItem('bgcolor');
            body.style.background = "blue";
        }
    })
})